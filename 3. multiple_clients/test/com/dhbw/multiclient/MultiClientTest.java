package com.dhbw.multiclient;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MultiClientTest {

    static final String IP = "127.0.0.1";
    static final int PORT = 8080;

    static final String HELLO = "hello";
    static final String WORLD = "world";
    static final String POINT = ".";
    static final String BYE = "bye";

    @Test
    void givenClient1_whenServerResponds_thenCorrect() {
        Client client1 = new Client();
        client1.startConnection(IP, PORT);

        Client client2 = new Client();
        client2.startConnection(IP, PORT);

        String client1Msg1 = client1.sendMessage(HELLO);

        String client2Msg1 = client2.sendMessage(HELLO);

        String client1Msg2 = client1.sendMessage(WORLD);
        String client1Terminate = client1.sendMessage(POINT);

        String client2Msg2 = client2.sendMessage(WORLD);
        String client2Terminate = client2.sendMessage(POINT);

        assertEquals(client2Msg1, HELLO);
        assertEquals(client1Msg2, WORLD);
        assertEquals(client1Terminate, BYE);

        assertEquals(client1Msg1, HELLO);
        assertEquals(client2Msg2, WORLD);
        assertEquals(client2Terminate, BYE);
    }
}
