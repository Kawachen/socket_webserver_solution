package com.dhbw.multiturn;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MultiTurnClientTest {

    static final String IP = "127.0.0.1";
    static final int PORT = 8080;

    static final String HELLO = "hello";
    static final String WORLD = "world";
    static final String POINT = ".";
    static final String EXCLAMATION_MARK = "!";
    static final String BYE = "good bye";

    private MultiTurnClient client;

    @BeforeEach
    void setup() {
        client = new MultiTurnClient();
        client.startConnection(IP, PORT);
    }

    @AfterEach
    void tearDown() {
        client.stopConnection();
    }

    @Test
    void givenGreetingClient_whenServerRespondsWhenStarted_thenCorrect() {
        String resp1 = client.sendMessage(HELLO);
        String resp2 = client.sendMessage(WORLD);
        String resp3 = client.sendMessage(EXCLAMATION_MARK);
        String resp4 = client.sendMessage(POINT);

        assertEquals(HELLO, resp1);
        assertEquals(WORLD, resp2);
        assertEquals(EXCLAMATION_MARK, resp3);
        assertEquals(BYE, resp4);
    }
}
