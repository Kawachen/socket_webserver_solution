package com.dhbw.simple;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GreetClientTest {
    @Test
    void givenGreetingClient_whenServerRespondsWhenStarted_thenCorrect() {
        var client = new GreetClient();
        client.startConnection("127.0.0.1", 8080);
        String response = client.sendMessage("hello server");
        client.stopConnection();
        assertEquals("hello client", response);
    }
}
